import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';

interface Node {
	children: Node[];
}

interface Named {
	id: string;
	short?: string;
	long?: string;
}

export interface Location extends Node, Named {
	cameras: Named[];
	children: Location[];
}

export interface Range {
	start: number;
	end: number;
}

interface LocationResult {
	location: Location;
	id: Range;
	short: Range;
	long: Range;
	ancestors: Location[];
}

interface Processed {
	string: string;
	removedIndex: number[];
}

@Injectable({providedIn: 'root'})
export class FacilityService {
	public readonly locations$: Observable<Location>;
	private readonly locationsBS: BehaviorSubject<Location>;

	constructor(private http: HttpClient) {
		this.locations$ = this.locationsBS = new BehaviorSubject<Location>(null);
	}

	public async getLocations(cached = false): Promise<Location> {
		// first, try the cache
		if (cached && this.locationsBS.value) {
			return Promise.resolve(this.locationsBS.value);
		}
		// otherwise, fetch remotely
		return this.http.get<Location>(environment.apiPath + 'face/locations').pipe(
			tap(locations => this.locationsBS.next(locations))
		).toPromise();
	}

	public lookup(id: string): Location {
		return walk(this.locationsBS.value, (l, _) => l, (l, _) => l.id === id)[0] || null;
	}
}

export function walk<N extends Node, R>(
	root: N,
	operator: (node: N, ancestors: N[]) => R,
	filter: (node: N, ancestors: N[]) => boolean = null,
	leavesOnly = false
): R[] {
	return _walk(root, operator, filter, leavesOnly, [], []);
}

function _walk(node, operator, filter, leavesOnly, ancestors, results) {
	if (!leavesOnly || !(node.children && node.children.length)) {
		if (!filter || filter(node, ancestors)) {
			results.push(operator(node, ancestors));
		}
	}
	ancestors.push(node);
	for (const child of node.children || []) {
		_walk(child, operator, filter, leavesOnly, ancestors, results);
	}
	ancestors.pop();
	return results;
}

export function searchLocation(rootLocation: Location, search: string, leavesOnly: boolean = false): LocationResult[] {
	return walk(rootLocation, (location, ancestors) => {
		return {
			location,
			id: findIndex(location.id, search),
			short: location.short ? findIndex(location.short, search) : null,
			long: location.long ? findIndex(location.long, search) : null,
			ancestors: ancestors.slice()
		};
	}, null, leavesOnly).filter((result) => result.id || result.short || result.long);
}

export function getLeaves(rootLocation: Location): any[] {
	return walk(rootLocation, (location, ancestors) => {
		return {
			location
		};
	}, null, true);
}

function findIndex(input: string, search: string): Range {
	search = processString(search).string;
	const cleanInput = processString(input);
	let index = cleanInput.string.indexOf(search);
	let indexLength = search.length;
	if (index !== -1) {
		for (const removedIndex of cleanInput.removedIndex) {
			if (removedIndex < index) {
				index++;
			}
			if (index + indexLength > removedIndex && removedIndex > index) {
				indexLength++;
			}
		}
	}
	return index === -1 ? null : {
		start: index,
		end: index + indexLength
	};
}

function processString(input: string): Processed {
	if( !input ) {
		return {
			string: '',
			removedIndex: []
		};
	}
	let stringStripped = input.toLowerCase().replace(/[^\w\s]|_/g, '?');
	const removedIndex = [];
	let z = stringStripped.indexOf('?');
	while (z >= 0) {
		removedIndex.push(z);
		z = stringStripped.indexOf('?', z + 1);
	}
	stringStripped = stringStripped.replace(/[^\w\s]|_/g, '');
	return {
		string: stringStripped,
		removedIndex
	};
}
