import { inject, TestBed } from '@angular/core/testing';
import { getLeaves, searchLocation } from './facilty.service';



describe('LocationsService', () => {
	let locations;
	let parsedLeaves;
	let nswSearch;
	let ancestors;
	beforeEach(() => {
		parsedLeaves = [
			{
				location: {
					id: 'isle',
					long: 'oof im a isle',
					short: 'is',
					cameras: [],
					children: []
				}
			},
			{
				location: {
					id: 'shop2',
					long: 'oof im a shop too',
					short: 'shoppy shop',
					cameras: [],
					children: []
				}
			},
			{
				location: {
					id: 'shop3',
					long: 'oof im a shop three',
					short: 'shoppy shop shop',
					cameras: [],
					children: []
				}
			},
			{
				location: {
					id: 'isle',
					long: 'oof im a isle',
					short: 'is',
					cameras: [],
					children: []
				}
			},
			{
				location: {
					id: 'shop2',
					long: 'oof im a shop too',
					short: 'shoppy shop',
					cameras: [],
					children: []
				}
			},
			{
				location: {
					id: 'shop3',
					long: 'of im a shop three brissy',
					short: 'o%o&f shoppy shop shoper',
					cameras: [],
					children: []
				}
			}
		];
		locations = {
			id: 'enver develop',
			long: 'enver Development',
			short: 'RB Dev',
			cameras: [],
			children: [{
				id: 'au',
				long: 'Australia',
				short: 'AU',
				cameras: [],
				children: [{
					id: 'nsw',
					long: 'New South Wales',
					short: 'NSW',
					cameras: [
						{
							id: 'sydney_face_cam_163',
							long: 'Sydney Lab Face Cam 163'
						},
						{
							id: 'sydney_face_cam_164',
							long: 'Sydney Lab Face Cam 164'
						}
					],
					children: [{
						id: 'Sydney',
						long: 'Wynyard sydney',
						short: 'Sydney Office',
						cameras: [],
						children: [{
							id: 'shop1',
							long: 'oof im a shop',
							short: 'shoppy',
							cameras: [],
							children: [{
								id: 'isle',
								long: 'oof im a isle',
								short: 'is',
								cameras: [],
								children: [],

							}],

						},
							{
								id: 'shop2',
								long: 'oof im a shop too',
								short: 'shoppy shop',
								cameras: [],
								children: [],

							},
							{
								id: 'shop3',
								long: 'oof im a shop three',
								short: 'shoppy shop shop',
								cameras: [],
								children: [],

							}],

					}],
				},
					{
						id: 'qld',
						long: 'Queensland',
						short: 'QLD',
						cameras: [
							{
								id: 'bris_face_cam_163',
								long: 'Bris Lab Face Cam 163'
							},
							{
								id: 'bris_face_cam_164',
								long: 'Bris Lab Face Cam 164'
							}
						],
						children: [{
							id: 'Bris',
							long: 'Somewhere in the vast nothingness',
							short: 'BR',
							cameras: [],
							children: [{
								id: 'shop1',
								long: 'oof im a shop',
								short: 'shoppy',
								cameras: [],
								children: [{
									id: 'isle',
									long: 'oof im a isle',
									short: 'is',
									cameras: [],
									children: [],

								}],

							},
								{
									id: 'shop2',
									long: 'oof im a shop too',
									short: 'shoppy shop',
									cameras: [],
									children: [],

								},
								{
									id: 'shop3',
									long: 'of im a shop three brissy',
									short: 'o%o&f shoppy shop shoper',
									cameras: [],
									children: [],

								}
							],

						}],
					},
				],
			}],
		};
		nswSearch = [
			{
				location: {
					id: 'nsw',
					long: 'New South Wales',
					short: 'NSW',
					cameras: [
						{
							id: 'sydney_face_cam_163',
							long: 'Sydney Lab Face Cam 163'
						},
						{
							id: 'sydney_face_cam_164',
							long: 'Sydney Lab Face Cam 164'
						}
					],
					children: [
						{
							id: 'Sydney',
							long: 'Wynyard sydney',
							short: 'Sydney Office',
							cameras: [],
							children: [
								{
									id: 'shop1',
									long: 'oof im a shop',
									short: 'shoppy',
									cameras: [],
									children: [
										{
											id: 'isle',
											long: 'oof im a isle',
											short: 'is',
											cameras: [],
											children: []
										}
									]
								},
								{
									id: 'shop2',
									long: 'oof im a shop too',
									short: 'shoppy shop',
									cameras: [],
									children: []
								},
								{
									id: 'shop3',
									long: 'oof im a shop three',
									short: 'shoppy shop shop',
									cameras: [],
									children: []
								}
							]
						}
					]
				},
				id: {
					start: 0,
					end: 3
				},
				short: {
					start: 0,
					end: 3
				},
				long: null,
				ancestors: [
					{
						id: 'enver develop',
						long: 'enver Development',
						short: 'RB Dev',
						cameras: [],
						children: [
							{
								id: 'au',
								long: 'Australia',
								short: 'AU',
								cameras: [],
								children: [
									{
										id: 'nsw',
										long: 'New South Wales',
										short: 'NSW',
										cameras: [
											{
												id: 'sydney_face_cam_163',
												long: 'Sydney Lab Face Cam 163'
											},
											{
												id: 'sydney_face_cam_164',
												long: 'Sydney Lab Face Cam 164'
											}
										],
										children: [
											{
												id: 'Sydney',
												long: 'Wynyard sydney',
												short: 'Sydney Office',
												cameras: [],
												children: [
													{
														id: 'shop1',
														long: 'oof im a shop',
														short: 'shoppy',
														cameras: [],
														children: [
															{
																id: 'isle',
																long: 'oof im a isle',
																short: 'is',
																cameras: [],
																children: []
															}
														]
													},
													{
														id: 'shop2',
														long: 'oof im a shop too',
														short: 'shoppy shop',
														cameras: [],
														children: []
													},
													{
														id: 'shop3',
														long: 'oof im a shop three',
														short: 'shoppy shop shop',
														cameras: [],
														children: []
													}
												]
											}
										]
									},
									{
										id: 'qld',
										long: 'Queensland',
										short: 'QLD',
										cameras: [
											{
												id: 'bris_face_cam_163',
												long: 'Bris Lab Face Cam 163'
											},
											{
												id: 'bris_face_cam_164',
												long: 'Bris Lab Face Cam 164'
											}
										],
										children: [
											{
												id: 'Bris',
												long: 'Somewhere in the vast nothingness',
												short: 'BR',
												cameras: [],
												children: [
													{
														id: 'shop1',
														long: 'oof im a shop',
														short: 'shoppy',
														cameras: [],
														children: [
															{
																id: 'isle',
																long: 'oof im a isle',
																short: 'is',
																cameras: [],
																children: []
															}
														]
													},
													{
														id: 'shop2',
														long: 'oof im a shop too',
														short: 'shoppy shop',
														cameras: [],
														children: []
													},
													{
														id: 'shop3',
														long: 'of im a shop three brissy',
														short: 'o%o&f shoppy shop shoper',
														cameras: [],
														children: []
													}
												]
											}
										]
									}
								]
							}
						]
					},
					{
						id: 'au',
						long: 'Australia',
						short: 'AU',
						cameras: [],
						children: [
							{
								id: 'nsw',
								long: 'New South Wales',
								short: 'NSW',
								cameras: [
									{
										id: 'sydney_face_cam_163',
										long: 'Sydney Lab Face Cam 163'
									},
									{
										id: 'sydney_face_cam_164',
										long: 'Sydney Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Sydney',
										long: 'Wynyard sydney',
										short: 'Sydney Office',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'oof im a shop three',
												short: 'shoppy shop shop',
												cameras: [],
												children: []
											}
										]
									}
								]
							},
							{
								id: 'qld',
								long: 'Queensland',
								short: 'QLD',
								cameras: [
									{
										id: 'bris_face_cam_163',
										long: 'Bris Lab Face Cam 163'
									},
									{
										id: 'bris_face_cam_164',
										long: 'Bris Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Bris',
										long: 'Somewhere in the vast nothingness',
										short: 'BR',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'of im a shop three brissy',
												short: 'o%o&f shoppy shop shoper',
												cameras: [],
												children: []
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				location: {
					id: 'Sydney',
					long: 'Wynyard sydney',
					short: 'Sydney Office',
					cameras: [],
					children: [
						{
							id: 'shop1',
							long: 'oof im a shop',
							short: 'shoppy',
							cameras: [],
							children: [
								{
									id: 'isle',
									long: 'oof im a isle',
									short: 'is',
									cameras: [],
									children: []
								}
							]
						},
						{
							id: 'shop2',
							long: 'oof im a shop too',
							short: 'shoppy shop',
							cameras: [],
							children: []
						},
						{
							id: 'shop3',
							long: 'oof im a shop three',
							short: 'shoppy shop shop',
							cameras: [],
							children: []
						}
					]
				},
				id: null,
				short: null,
				long: {
					start: 43,
					end: 46
				},
				ancestors: [
					{
						id: 'enver develop',
						long: 'enver Development',
						short: 'RB Dev',
						cameras: [],
						children: [
							{
								id: 'au',
								long: 'Australia',
								short: 'AU',
								cameras: [],
								children: [
									{
										id: 'nsw',
										long: 'New South Wales',
										short: 'NSW',
										cameras: [
											{
												id: 'sydney_face_cam_163',
												long: 'Sydney Lab Face Cam 163'
											},
											{
												id: 'sydney_face_cam_164',
												long: 'Sydney Lab Face Cam 164'
											}
										],
										children: [
											{
												id: 'Sydney',
												long: 'Wynyard sydney',
												short: 'Sydney Office',
												cameras: [],
												children: [
													{
														id: 'shop1',
														long: 'oof im a shop',
														short: 'shoppy',
														cameras: [],
														children: [
															{
																id: 'isle',
																long: 'oof im a isle',
																short: 'is',
																cameras: [],
																children: []
															}
														]
													},
													{
														id: 'shop2',
														long: 'oof im a shop too',
														short: 'shoppy shop',
														cameras: [],
														children: []
													},
													{
														id: 'shop3',
														long: 'oof im a shop three',
														short: 'shoppy shop shop',
														cameras: [],
														children: []
													}
												]
											}
										]
									},
									{
										id: 'qld',
										long: 'Queensland',
										short: 'QLD',
										cameras: [
											{
												id: 'bris_face_cam_163',
												long: 'Bris Lab Face Cam 163'
											},
											{
												id: 'bris_face_cam_164',
												long: 'Bris Lab Face Cam 164'
											}
										],
										children: [
											{
												id: 'Bris',
												long: 'Somewhere in the vast nothingness',
												short: 'BR',
												cameras: [],
												children: [
													{
														id: 'shop1',
														long: 'oof im a shop',
														short: 'shoppy',
														cameras: [],
														children: [
															{
																id: 'isle',
																long: 'oof im a isle',
																short: 'is',
																cameras: [],
																children: []
															}
														]
													},
													{
														id: 'shop2',
														long: 'oof im a shop too',
														short: 'shoppy shop',
														cameras: [],
														children: []
													},
													{
														id: 'shop3',
														long: 'of im a shop three brissy',
														short: 'o%o&f shoppy shop shoper',
														cameras: [],
														children: []
													}
												]
											}
										]
									}
								]
							}
						]
					},
					{
						id: 'au',
						long: 'Australia',
						short: 'AU',
						cameras: [],
						children: [
							{
								id: 'nsw',
								long: 'New South Wales',
								short: 'NSW',
								cameras: [
									{
										id: 'sydney_face_cam_163',
										long: 'Sydney Lab Face Cam 163'
									},
									{
										id: 'sydney_face_cam_164',
										long: 'Sydney Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Sydney',
										long: 'Wynyard sydney',
										short: 'Sydney Office',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'oof im a shop three',
												short: 'shoppy shop shop',
												cameras: [],
												children: []
											}
										]
									}
								]
							},
							{
								id: 'qld',
								long: 'Queensland',
								short: 'QLD',
								cameras: [
									{
										id: 'bris_face_cam_163',
										long: 'Bris Lab Face Cam 163'
									},
									{
										id: 'bris_face_cam_164',
										long: 'Bris Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Bris',
										long: 'Somewhere in the vast nothingness',
										short: 'BR',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'of im a shop three brissy',
												short: 'o%o&f shoppy shop shoper',
												cameras: [],
												children: []
											}
										]
									}
								]
							}
						]
					},
					{
						id: 'nsw',
						long: 'New South Wales',
						short: 'NSW',
						cameras: [
							{
								id: 'sydney_face_cam_163',
								long: 'Sydney Lab Face Cam 163'
							},
							{
								id: 'sydney_face_cam_164',
								long: 'Sydney Lab Face Cam 164'
							}
						],
						children: [
							{
								id: 'Sydney',
								long: 'Wynyard sydney',
								short: 'Sydney Office',
								cameras: [],
								children: [
									{
										id: 'shop1',
										long: 'oof im a shop',
										short: 'shoppy',
										cameras: [],
										children: [
											{
												id: 'isle',
												long: 'oof im a isle',
												short: 'is',
												cameras: [],
												children: []
											}
										]
									},
									{
										id: 'shop2',
										long: 'oof im a shop too',
										short: 'shoppy shop',
										cameras: [],
										children: []
									},
									{
										id: 'shop3',
										long: 'oof im a shop three',
										short: 'shoppy shop shop',
										cameras: [],
										children: []
									}
								]
							}
						]
					}
				]
			}
		];

		ancestors = [
			{
				id: 'enver develop',
				long: 'enver Development',
				short: 'RB Dev',
				cameras: [],
				children: [
					{
						id: 'au',
						long: 'Australia',
						short: 'AU',
						cameras: [],
						children: [
							{
								id: 'nsw',
								long: 'New South Wales',
								short: 'NSW',
								cameras: [
									{
										id: 'sydney_face_cam_163',
										long: 'Sydney Lab Face Cam 163'
									},
									{
										id: 'sydney_face_cam_164',
										long: 'Sydney Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Sydney',
										long: 'Wynyard sydney',
										short: 'Sydney Office',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'oof im a shop three',
												short: 'shoppy shop shop',
												cameras: [],
												children: []
											}
										]
									}
								]
							},
							{
								id: 'qld',
								long: 'Queensland',
								short: 'QLD',
								cameras: [
									{
										id: 'bris_face_cam_163',
										long: 'Bris Lab Face Cam 163'
									},
									{
										id: 'bris_face_cam_164',
										long: 'Bris Lab Face Cam 164'
									}
								],
								children: [
									{
										id: 'Bris',
										long: 'Somewhere in the vast nothingness',
										short: 'BR',
										cameras: [],
										children: [
											{
												id: 'shop1',
												long: 'oof im a shop',
												short: 'shoppy',
												cameras: [],
												children: [
													{
														id: 'isle',
														long: 'oof im a isle',
														short: 'is',
														cameras: [],
														children: []
													}
												]
											},
											{
												id: 'shop2',
												long: 'oof im a shop too',
												short: 'shoppy shop',
												cameras: [],
												children: []
											},
											{
												id: 'shop3',
												long: 'of im a shop three brissy',
												short: 'o%o&f shoppy shop shoper',
												cameras: [],
												children: []
											}
										]
									}
								]
							}
						]
					}
				]
			},
			{
				id: 'au',
				long: 'Australia',
				short: 'AU',
				cameras: [],
				children: [
					{
						id: 'nsw',
						long: 'New South Wales',
						short: 'NSW',
						cameras: [
							{
								id: 'sydney_face_cam_163',
								long: 'Sydney Lab Face Cam 163'
							},
							{
								id: 'sydney_face_cam_164',
								long: 'Sydney Lab Face Cam 164'
							}
						],
						children: [
							{
								id: 'Sydney',
								long: 'Wynyard sydney',
								short: 'Sydney Office',
								cameras: [],
								children: [
									{
										id: 'shop1',
										long: 'oof im a shop',
										short: 'shoppy',
										cameras: [],
										children: [
											{
												id: 'isle',
												long: 'oof im a isle',
												short: 'is',
												cameras: [],
												children: []
											}
										]
									},
									{
										id: 'shop2',
										long: 'oof im a shop too',
										short: 'shoppy shop',
										cameras: [],
										children: []
									},
									{
										id: 'shop3',
										long: 'oof im a shop three',
										short: 'shoppy shop shop',
										cameras: [],
										children: []
									}
								]
							}
						]
					},
					{
						id: 'qld',
						long: 'Queensland',
						short: 'QLD',
						cameras: [
							{
								id: 'bris_face_cam_163',
								long: 'Bris Lab Face Cam 163'
							},
							{
								id: 'bris_face_cam_164',
								long: 'Bris Lab Face Cam 164'
							}
						],
						children: [
							{
								id: 'Bris',
								long: 'Somewhere in the vast nothingness',
								short: 'BR',
								cameras: [],
								children: [
									{
										id: 'shop1',
										long: 'oof im a shop',
										short: 'shoppy',
										cameras: [],
										children: [
											{
												id: 'isle',
												long: 'oof im a isle',
												short: 'is',
												cameras: [],
												children: []
											}
										]
									},
									{
										id: 'shop2',
										long: 'oof im a shop too',
										short: 'shoppy shop',
										cameras: [],
										children: []
									},
									{
										id: 'shop3',
										long: 'of im a shop three brissy',
										short: 'o%o&f shoppy shop shoper',
										cameras: [],
										children: []
									}
								]
							}
						]
					}
				]
			},
			{
				id: 'qld',
				long: 'Queensland',
				short: 'QLD',
				cameras: [
					{
						id: 'bris_face_cam_163',
						long: 'Bris Lab Face Cam 163'
					},
					{
						id: 'bris_face_cam_164',
						long: 'Bris Lab Face Cam 164'
					}
				],
				children: [
					{
						id: 'Bris',
						long: 'Somewhere in the vast nothingness',
						short: 'BR',
						cameras: [],
						children: [
							{
								id: 'shop1',
								long: 'oof im a shop',
								short: 'shoppy',
								cameras: [],
								children: [
									{
										id: 'isle',
										long: 'oof im a isle',
										short: 'is',
										cameras: [],
										children: []
									}
								]
							},
							{
								id: 'shop2',
								long: 'oof im a shop too',
								short: 'shoppy shop',
								cameras: [],
								children: []
							},
							{
								id: 'shop3',
								long: 'of im a shop three brissy',
								short: 'o%o&f shoppy shop shoper',
								cameras: [],
								children: []
							}
						]
					}
				]
			},
			{
				id: 'Bris',
				long: 'Somewhere in the vast nothingness',
				short: 'BR',
				cameras: [],
				children: [
					{
						id: 'shop1',
						long: 'oof im a shop',
						short: 'shoppy',
						cameras: [],
						children: [
							{
								id: 'isle',
								long: 'oof im a isle',
								short: 'is',
								cameras: [],
								children: []
							}
						]
					},
					{
						id: 'shop2',
						long: 'oof im a shop too',
						short: 'shoppy shop',
						cameras: [],
						children: []
					},
					{
						id: 'shop3',
						long: 'of im a shop three brissy',
						short: 'o%o&f shoppy shop shoper',
						cameras: [],
						children: []
					}
				]
			}
		];

	});

	describe('Locations', () => {

		it('should get leaves', inject([],
			() => {
				const leaves = getLeaves(locations);
				expect(leaves).toEqual(parsedLeaves);
			}));

		it('should search for substrings', inject([],
			() => {
				const results = searchLocation(locations, 'oof');
				// expect that we handle special characters and the index takes this into account
				expect(results[7].location.short).toEqual('o%o&f shoppy shop shoper');
				expect(results[7].short).toEqual({start: 0, end: 5});
				expect(results[5].location.long).toEqual('oof im a isle');
				expect(results[5].long).toEqual({start: 0, end: 3});
				// expect that we track ancestors
				expect(results[7].ancestors).toEqual(ancestors);
			}));

		it('should search all nodes by default', inject([],
			() => {
				const results = searchLocation(locations, 'nsw');
				expect(results).toEqual(nswSearch);
			}));

		it('should search leaves only optionally', inject([],
			() => {

				let results = searchLocation(locations, 'nsw', true);
				expect(results.length).toEqual(0);
				results = searchLocation(locations, 'o#o#@%&#%#&*#*@#$":>??>":{f', true);
				expect(results.length).toEqual(6);

			}));

		it('search should only return short or long if they exist', inject([],
			() => {
				// should return all if exist
				let results = searchLocation(locations, 'sydney');
				expect(results[0].id).toEqual({
					start: 0,
					end: 6
				});
				expect(results[0].short).toEqual({
					start: 0,
					end: 6
				});
				expect(results[0].long).toEqual({
					start: 36,
					end: 42
				});
				// should return long if string not present in short
				results = searchLocation(locations, 'isle');
				expect(results[0].long).toEqual({
					start: 9,
					end: 13
				});
				expect(results[0].short).toBe(null);
				results = searchLocation(locations, 'bris');
				// should return id even if not on long or short
				expect(results[0].id).toEqual({
					start: 0,
					end: 4
				});
				expect(results[0].short).toBe(null);
				expect(results[0].long).toBe(null);
			}));

	});
});
